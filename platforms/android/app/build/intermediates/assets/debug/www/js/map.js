// Note: This example requires that you consent to location sharing when
// prompted by your browser. If you see the error "The Geolocation service
// failed.", it means you probably did not give permission for the browser to
// locate you.
var map, infoWindow, geocoder;

//Main Function
function initMap() {
    // Location showed before geolocation is activate in the browser
    map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: 47.213, lng: -1.528},
        zoom: 15
    });
    infoWindow = new google.maps.InfoWindow;
    geocoder = new google.maps.Geocoder;

    // Try HTML5 geolocation.
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (position) {
            var pos = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };

            geocodeLatLng(geocoder, map, infoWindow, pos.lat, pos.lng);

            // Location object
            var locations = {
                lat: pos.lat,
                lng: pos.lng,
                date: getDate(),
                hour: getHour(),
                address: localStorage.getItem("address")
            };

            // We convert the object in JSON and export it in localStorage
            var oldItems = JSON.parse(localStorage.getItem('itemsArray')) || [];
            oldItems.push(locations);

            // We display the templates filled with data
            displayList(oldItems.reverse());

            // Map infos
            infoWindow.setPosition(pos);
            infoWindow.setContent('Votre position');
            infoWindow.open(map);
            map.setCenter(pos);
        }, function () {
            handleLocationError(true, infoWindow, map.getCenter());
        });
    } else {
        // Browser doesn't support Geolocation
        handleLocationError(false, infoWindow, map.getCenter());
    }
}

//Error support
function handleLocationError(browserHasGeolocation, infoWindow, pos) {
    infoWindow.setPosition(pos);
    infoWindow.setContent(browserHasGeolocation ?
        'Error: The Geolocation service failed.' :
        'Error: Your browser doesn\'t support geolocation.');
    infoWindow.open(map);
}


//Convert lat and long to address and store it into localStorage if success
function geocodeLatLng(geocoder, map, infoWindow, lat, long) {
    var latlng = {lat: parseFloat(lat), lng: parseFloat(long)};
    geocoder.geocode({'location': latlng}, function (results, status) {
        if (status === 'OK') {
            if (results[0]) {
                // If address is OK
                // Then address is put in localStorage
                localStorage.setItem("address", results[0].formatted_address);
            } else {
                // If it can't find the address
                document.getElementById("addr").innerHTML = "Addresse introuvable";
            }
        } else {
            // If nothing worked, it display an error message + status of the error (founded by Google)
            window.alert('Geocoder failed due to: ' + status);
        }
    });
}

//Initialize hour
function addZero(i) {
    if (i < 10) {
        i = "0" + i;
    }
    return i;
}

//Return actual hour
function getHour() {
    var d = new Date();
    var h = addZero(d.getHours());
    var m = addZero(d.getMinutes());
    var s = addZero(d.getSeconds());
    return h + ":" + m + ":" + s;
}

//Return actual date
function getDate() {
    var d = new Date();
    var day = d.getDate();
    var month = d.getMonth();
    var year = d.getFullYear();
    return day + "/" + month + "/" + year;
}

// Display sections of history
function displayList(oldItems) {

    // Get the data in localStorage
    JSONitems = JSON.stringify(oldItems);
    localStorage.setItem('itemsArray', JSONitems);

    for (var I = 0; I < oldItems.length; I++) {

        // Remove "Veuillez activer la géolocalisation." when geolocation is activate
        var activation = document.getElementById("activation");
        activation.style.display = "none";

        // Section template :
        document.getElementById('positions').innerHTML +=
            '<section> ' +
            '<div class="date_heure"> ' +
            '<p id="date">' + oldItems[I].date + '</p> ' +
            '<p id="hour">' + oldItems[I].hour + '</p>' +
            '</div>' +
            '<p id="addr">' + oldItems[I].address + '</p>' +
            '<div class="latlong">' +
            '<p>lat :</p> <p id="lat">' + oldItems[I].lat +
            '</p>' +
            '<p>long :</p> <p id="long">' + oldItems[I].lng +
            '</div>' +
            '<hr>' +
            ' </section>';

        // Control everything went OK
        console.log("Location sent into html.");
    }

}









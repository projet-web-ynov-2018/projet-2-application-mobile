function displayHistory() {
    var display = document.getElementById("history");
    if (display.className === "animated fadeInUpBig") {
        display.classList.remove("fadeInUpBig");
        display.classList.add("fadeOutDownBig");
    } else {
        display.classList.remove("fadeOutDownBig");
        display.classList.add("fadeInUpBig");
        display.style.display = "block";
    }
}